/*
SKETCH 2

WS Server wirh Kafka consumer.
*/


var util  = require('util');
var Kafka = require('no-kafka');
var Producer = Kafka.Producer;
var connect = require('connect');
var st = require('st');


var ws = require("nodejs-websocket");
 

var producer = new Producer({
     connectionString: '52.166.114.79:9092'
});

var consumer = new Kafka.SimpleConsumer({
        connectionString: '52.166.114.79:9092'
});



// Scream server example: "hi" -> "HI!!!" 
var server = ws.createServer(function (conn) {
    console.log("New connection")
    conn.on("text", function (str) {

        console.log("Received "+str)
        conn.sendText(str.toUpperCase()+"!!!")






                        return producer.init().then(function(){
                          return producer.send({
                              topic: 'test3',
                              message: {
                                  value : 'CHAT CLIENT OUTPUT TO KAFKA CONSUMER--> ' + str
                              }
                          });
                        });





    })
    conn.on("close", function (code, reason) {
        console.log("Connection closed")
    })



                        /*KAFKA CONSUMER section*/

                        // data handler function can return a Promise
                        var dataHandler = function (messageSet, topic, partition) {
                            messageSet.forEach(function (m) {
                                //console.log(topic, partition, m.offset, m.message.value.toString('utf8'));
                                //io.emit( 'chat message',  'KAFKA SUBSCRIBER OUTPUT TO CHAT CLIENT---> ' + m.message.value.toString('utf8') );
                               fullMessage =  m.message.value.toString('utf8');

                                conn.sendText(fullMessage.toUpperCase()+"!!!")

                            });
                        };

                        return consumer.init().then(function () {
                            // Subscribe partitons 0 and 1 in a topic by [0,1,2]:
                            return consumer.subscribe('test3', [0], dataHandler);
                        });


}).listen(9000)
 
